def end():
    print('Zamykam program.')
    exit()

def no_comand(ACCESIBLE_COMAND):   
    print('Brak komendy.')
    print('Komendy które można używać:')
    print('\n'.join(ACCESIBLE_COMAND))

def wrong_comand(ACCESIBLE_COMAND):
    print('Komenda nie pasuje do żadnej z zapisanych.')
    print('Komendy które można używać:')
    print('\n'.join(ACCESIBLE_COMAND))

def test_comand_task(task, ACCESIBLE_COMAND):
    if task == '':
        no_comand(ACCESIBLE_COMAND)
        input('wciśnij <ENTER> aby spróbować ponownie. \n')
            
    elif task not in ACCESIBLE_COMAND :
        wrong_comand(ACCESIBLE_COMAND)
        input('wciśnij <ENTER> aby spróbować ponownie. \n')