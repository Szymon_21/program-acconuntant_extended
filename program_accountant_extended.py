'''
Napisz program (accountant.py), który będzie rejestrował operacje na koncie firmy i stan magazynu.
Program jest wywoływany w następujący sposób:
a) python accountant.py saldo <int wartosc> <str komentarz>
b) python accountant.py sprzedaż <str identyfikator produktu> <int cena> <int liczba sprzedanych>
c) python accountant.py zakup <str identyfikator produktu> <int cena> <int liczba zakupionych>
d) python accountant.py konto
e) python accountant.py magazyn <str identyfikator produktu 1> <str identyfikator produktu 2> <str identyfikator produktu 3> ...
f) python accountant.py przegląd

Działanie programu będzie zależne od podanych argumentów
Niezależnie od trybu program zawsze będzie działał w następujący sposób
I. Program pobierze rodzaj akcji (ciąg znaków). Dozwolone akcje to "saldo", zakup", "sprzedaż". Jeśli użytkownik wprowadzi inną akcję, program powinien zwrócić błąd i zakończyć działanie.
saldo: program pobiera dwie linie: zmiana na koncie firmy wyrażona w groszach (int) (może być ujemna) oraz komentarz do zmiany (str)
zakup: program pobiera trzy linie: identyfikator produktu (str), cena jednostkowa (int) i liczba sztuk (int). Program odejmuje z salda cenę jednostkową pomnożoną przez liczbę sztuk. Jeśli saldo po zmianie jest ujemne, cena jest ujemna bądź liczba sztuk jest mniejsza od zero program zwraca błąd. Program podnosi stan magazynowy zakupionego towaru
sprzedaż: program pobiera trzy linie: identyfikator produktu (str), cena jednostkowa (int), liczba sztuk (int). Program dodaje do salda cenę jednostkową pomnożoną razy liczbę sztuk. Jeśli na magazynie nie ma wystarczającej liczby sztuk, cena jest ujemna bądź liczba sztuk sprzedanych jest mniejsza od zero program zwraca błąd. Program obniża stan magazynowy zakupionego towaru.
stop: program przechodzi do kroku IV
II. Program zapamiętuje każdą wprowadzoną linię
III. Program wraca do kroku I
IV. W zależności od wywołania:
a) b) c) program dodaje do historii podane argumenty tak, jakby miały być wprowadzone przez standardowe wejście, przechodzi do kroku V
d) program wypisuje na standardowe wyjście stan konta po wszystkich akcjach, kończy działanie
e) program wypisuje stany magazynowe dla podanych produktów, w formacie: <id produktu>: <stan> w nowych liniach i kończy działanie:
f) Program wypisuje wszystkie akcje zapisane pod indeksami w zakresie [od, do] (zakresy włącznie)
V. Program wypisuje wszystkie podane parametry w formie identycznej, w jakiej je pobrał.
'''
'''
rozszerzmy nasz program do systemu księgowego. Zamiast pisać i czytać ze standardowego wejścia/wyjścia rezultat czyta i zapisuje do podanego pliku.
Program jest wywoływany w następujący sposób:
a) python saldo.py <plik><int wartosc> <str komentarz>
b) python sprzedaz.py <plik><str identyfikator produktu> <int cena> <int liczba sprzedanych>
c) python zakup.py <plik> <str identyfikator produktu> <int cena> <int liczba zakupionych>
d) python konto.py <plik>
e) python magazyn.py <plik><str identyfikator produktu 1> <str identyfikator produktu 2> <str identyfikator produktu 3> ...
f) python przeglad.py <plik>

Wymienione pliki powinny importować i korzystać co najmniej z jednej zaimportowanej funkcji / lub klasy.
'''
from ast import Pass, Return
import pickle

from function import end
from function import no_comand
from function import wrong_comand
from function import test_comand_task

print(30*'#')
print('PROGRAM ACCOUNTANT EXTENDED')
print(30*'#')
print('\n')


acount = {}
money = 0

history_of_buy = {}
history_of_sell = {}

depot = {}
depot_moment = []
depot_product = []


class balance:
    def __init__(self, value, coment):
        self.value = value
        self.coment = coment

    def save_to_dict(self):
        try:
            money_save = open("money.txt", "r")
            money = money_save.readline()
            money_save.close()

            a_file_balance = open("balance.p", "rb")
            acount = pickle.load(a_file_balance)
            a_file_balance.close()

            self.value = int(self.value) * 100
            money = int(money)
            money_action = money + self.value
            acount[self.coment] = self.value
            print(f'na koncie mamy {money_action} groszy')

            money_save = open("money.txt", "w")
            money_save.write(str(money_action))
            money_save.close()

            a_file = open("balance.p", "wb")
            pickle.dump(acount, a_file)
            a_file.close()

        except EOFError:
            self.value = int(self.value) * 100
            money = int(money)
            money_action = money + self.value
            acount[self.coment] = self.value
            print(f'na koncie mamy {money_action} groszy')

            money_save = open("money.txt", "w")
            money_save.write(str(money_action))
            money_save.close()

            a_file = open("balance.p", "wb")
            pickle.dump(acount, a_file)
            a_file.close()

class sell:
    def __init__(self, product_id, price, number):
        self.product_id = product_id
        self.price = price
        self.number = number
    
    def save_to_dict(self):
        try:
            money_save = open("money.txt", "r")
            money = money_save.readline()
            money_save.close()
            
            a_file_sell = open("depot.p", "rb")
            depot = pickle.load(a_file_sell)
            a_file_sell.close()
            
            warehouse_save = open("warehouse.p", "rb")
            depot_product = pickle.load(warehouse_save)
            warehouse_save.close()
            
            self.price = int(self.price)
            self.number = int(self.number)
            
            price_of_all = self.price * self.number
        
            action1 = money + price_of_all
            
            if depot(self.product_id) < self.number:
                print('Nie ma tyle towaru na magazynie!')
                print('Spróbuj inną kwotę.')
                
            else:
                numer_of_product_in_depot = depot(self.product_id)
                
                action_sell = numer_of_product_in_depot - self.number
                
                depot[self.product_id] = action_sell
                
                depot_product.remove(self.product_id)
                
                money = action1
                
                money_save = open("money.txt", "w")
                money_save.write(money)
                money_save.close()
                
                a_file_sell = open("depot.p", "wb")
                pickle.dump(depot, a_file_sell)
                a_file_sell.close()
                
                warehouse_save = open("warehouse.p", "wb")
                pickle.dump(depot_product, warehouse_save)
                warehouse_save.close()
                
        except EOFError:
            self.price = int(self.price)
            self.number = int(self.number)
            
            a_file_sell = open("depot.p", "rb")
            depot = pickle.load(a_file_sell)
            a_file_sell.close()
            
            action1 = money + (self.price * self.number)
            
            price_of_all = self.price * self.number
            
            if depot(self.product_id) < self.number:
                print('Nie ma tyle towaru na magazynie!')
                print('Spróbuj inną kwotę.')
                
            else:
                numer_of_product_in_depot = depot(self.product_id)
                
                action_sell = numer_of_product_in_depot - self.number
                
                depot[self.product_id] = action_sell
                
                depot_product.remove(self.product_id)
                
                money = action1
                
                money_save = open("money.txt", "w")
                money_save.write(money)
                money_save.close()
                
                a_file_sell = open("depot.p", "wb")
                pickle.dump(depot, a_file)
                a_file.close()
                
                warehouse_save = open("warehouse.p", "wb")
                pickle.dump(depot_product, warehouse_save)
                warehouse_save.close()
    
class buy:
    def __init__(self, product_id, price, number):
        self.product_id = product_id
        self.price = price
        self.number = number

    def save_to_dict(self):
        try:
            self.price = int(self.price)
            self.number = int(self.number)
            action2 = money - (self.price * self.number)
            money = action2
            depot_moment.append(self.price, self.number)
            depot[self.product_id] = depot_moment
            total_money_for_products = self.price * self.number
            history_of_buy[self.product_id] = total_money_for_products
            
        except:
            self.price = int(self.price)
            self.number = int(self.number)
            action2 = money - (self.price * self.number)
            money = action2
            depot_moment.append(self.price, self.number)
            depot[self.product_id] = depot_moment
            total_money_for_products = self.price * self.number
            history_of_buy[self.product_id] = total_money_for_products

class warehouse:
    def __init__(self, product_id):
        self.product_id = product_id

    def save_to_dict(self):
        try:
            warehouse_save = open("warehouse.p", "rb")
            depot_product = pickle.load(warehouse_save)
            warehouse_save.close()
            
            depot_product.append(product_id)

            warehouse_save = open("warehouse.p", "wb")
            pickle.dump(depot_product, warehouse_save)
            warehouse_save.close()

        except EOFError:
            depot_product.append(product_id)
            
            warehouse_save = open("warehouse.p", "wb")
            pickle.dump(depot_product, warehouse_save)
            warehouse_save.close()

class overview:
    try:
        a_file = open("balance.p", "rb")
        acount = pickle.load(a_file)
        a_file.close()
        print(acount)

        money_save = open("money.txt", "r")
        money = money_save.readline()
        money_save.close()
        print(money)
        
    except:
        pass
    
while True:
    
    ACCESIBLE_COMAND = ("saldo", "zakup", "sprzedaż",
                        "konto", "magazyn", "przegląd",
                        "koniec")
    
    print('\n')
    print(11 * '#')
    print('MENU GŁÓWNE')
    print(11 * '#')
    print('\n')

    print(f'Dostepne komendy: {ACCESIBLE_COMAND}')
    task = input('Podaj komendę: ')

    test_comand_task(task, ACCESIBLE_COMAND)

    if task == "koniec":
        end()

    elif task == "saldo" :
        print(15 * '-')
        print('SALDO')
        print(15 * '-')

        while True:
            value = input('Podaj Kwotę albo nacisnij enter aby wyjść: ')
            value = int(value)
            
            if value == '':
                break
            
            else:
                try:
                    coment = input('Podaj komentarz do kwoty: ')
                    balance_input = balance(value, coment)
                    balance_input.save_to_dict()
                    continue
                
                except ValueError:
                    print('Podana wartość nie jest liczbą.')
                    break
            

    elif task == "sprzedaż":
        print(15 * '-')
        print('SPRZEDAŻ')
        print(15 * '-')

        while True:
            product_id = input('Podaj identyfikator produktu albo nacisnij enter aby wyjść: ')
            if product_id == '':
                break
            
            else:
                if product_id not in depot:
                    print('\n')
                    print('Nie można sprzedać takiego produktu, ponieważ nie ma go w magazynie.')
                    print('Powrót do głównego menu.')
                    print('\n')
                    break
                
                else:
                    price = input('Podaj kwotę sprzedanych produktów: ')
                    number = input('podaj ilość sprzedanych produktów: ')
                    sell_input = sell(product_id, price, number)
                    sell_input.save_to_dict()
                    continue
            

    elif task == "zakup":
        print(15 * '-')
        print('ZAKUP')
        print(15 * '-')

        while True:
            product_id = input('Podaj identyfikator produktu albo nacisnij enter aby wyjść: ')
            if product_id == '':
                break
            
            else:
                price = input('Podaj kwotę kupionych produktów: ')
                number = input('podaj ilość kupionych produktów: ')
                buy_input = buy(product_id, price, number)
                buy_input.save_to_dict()
                continue

    elif task == "konto":
        print(15 * '-')
        print('KONTO')
        print(15 * '-')

        a_file = open("balance.p", "rb")
        acount = pickle.load(a_file)
        a_file.close()
        print(acount)

        money_save = open("money.txt", "r")
        money = money_save.readline()
        money_save.close()
        print(money)

    elif task == "magazyn":
        print(15 * '-')
        print('MAGAZYN')
        print(15 * '-')

        while True:
            product_id = input('Podaj nazwę produktu: ')
            if product_id == '':
                break
            
            else:
                warehouse_input = warehouse(product_id)
                warehouse_input.save_to_dict()
                continue
                    

    elif task == "przegląd":
        pass

            